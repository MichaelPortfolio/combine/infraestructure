####################################################
#? General
####################################################
variable "region" {
    type = string
}

####################################################
#? Domain
####################################################
variable "domain" {
    type = string
}

####################################################
#? VPC
####################################################
variable "vpc_name" {
    type = string
}
variable "vpc_ip_ranger" {
    type = string
}

####################################################
#? Kubernetes Cluster
####################################################
variable "cluster_name" {
    type = string
}
variable "cluster_version" {
    type = string
}
variable "cluster_node_name" {
    type = string
}
variable "cluster_node_size" {
    type = string
}
variable "cluster_node_count" {
    type = number
}