resource "digitalocean_vpc" "main" {
  region   = var.region
  
  name     = var.vpc_name
  ip_range = var.vpc_ip_ranger
}

resource "digitalocean_domain" "main" {
  name       = var.domain
}

resource "digitalocean_kubernetes_cluster" "main" {
  region = var.region

  name   = var.cluster_name
  version = var.cluster_version
  vpc_uuid = digitalocean_vpc.main.id

  node_pool {
    name       = var.cluster_node_name
    size       = var.cluster_node_size
    node_count = var.cluster_node_count
  }
}