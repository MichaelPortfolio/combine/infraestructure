terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "MichaelPortfolio"
    workspaces {
      name = "Combine"
    }
  }
  required_version = ">= 1.0.6"
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = ">= 2.11.1"
    }
  }
}